package util

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"

	"gitlab.com/yosiaagustadewa/qsl-util/helper"
	"gopkg.in/mgo.v2/bson"
)

type BodyParser struct {
	ByteResult []byte
	Err        error
}

func (bParser *BodyParser) CleanByte() *BodyParser {
	bParser.ByteResult = []byte{}
	return bParser
}

func (bParser *BodyParser) ParseRequestBody(rc *io.ReadCloser) *BodyParser {
	bParser.ByteResult, bParser.Err = ioutil.ReadAll(*rc)

	*rc = ioutil.NopCloser(bytes.NewBuffer(bParser.ByteResult))

	if bParser.Err != nil {
		return bParser
	}

	return bParser
}

func (bParser *BodyParser) CheckExistingParam(targetKey ...string) *BodyParser {
	if bParser.Err != nil {
		return bParser
	}

	var bsonBody bson.M
	bParser.Err = bson.UnmarshalJSON(bParser.ByteResult, &bsonBody)

	if bParser.Err != nil {
		return bParser
	}

	var bsonBodyPV bson.M
	bParser.Err = helper.PairValues(bsonBody, &bsonBodyPV)

	if bParser.Err != nil {
		return bParser
	}

	for _, tKey := range targetKey {
		ctr := 0
		exist := false
		for bKey := range bsonBodyPV {
			ctr++
			if tKey == bKey {
				exist = true
				break
			} else if ctr == len(bsonBodyPV) && exist == false {
				bParser.Err = errors.New("Required param " + tKey)
				return bParser
			} else {
				continue
			}
		}
	}
	return bParser
}

func (bParser *BodyParser) Decode(result interface{}) *BodyParser {
	if bParser.Err != nil {
		return bParser
	}

	bParser.Err = json.Unmarshal(bParser.ByteResult, &result)

	if bParser.Err != nil {
		return bParser
	}
	return bParser
}
