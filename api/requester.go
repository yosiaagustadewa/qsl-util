package api

import (
	"bytes"
	"errors"
	"io"
	"net/http"

	"gitlab.com/yosiaagustadewa/qsl-util/helper"

	globalModel "gitlab.com/yosiaagustadewa/qsl-util/model"

	"github.com/gin-gonic/gin"
	"gitlab.com/yosiaagustadewa/qsl-util"
	"go.mongodb.org/mongo-driver/bson"
)

type Requester struct {
	c        *gin.Context
	BParser  util.BodyParser
	response *Response

	Client  http.Client
	httpReq *http.Request
}

func (r *Requester) Use(c *gin.Context) *Requester {
	r.c = c
	return r
}

func (r *Requester) Request(method, url string, body ...interface{}) *Requester {

	r.BParser.CleanByte()
	var reqBody io.Reader

	if body != nil && len(body) != 0 {
		switch body[0].(type) {
		case []byte:
			reqBody = bytes.NewBuffer(body[0].([]byte))
		default:
			var byteBody []byte
			byteBody, r.BParser.Err = bson.MarshalExtJSON(body[0], false, false)
			reqBody = bytes.NewBuffer(byteBody)
		}

	}

	r.httpReq, r.BParser.Err = http.NewRequestWithContext(r.c.Request.Context(), method, url, reqBody)
	if r.BParser.Err != nil {
		return r
	}
	r.httpReq.Header.Set("Content-Type", "application/json")

	r.httpReq.Response, r.BParser.Err = r.Client.Do(r.httpReq)

	if r.httpReq.Response == nil {
		r.BParser.Err = errors.New("nil response")
		return r
	}

	if r.httpReq.Response.StatusCode == http.StatusNotFound {
		r.BParser.Err = errors.New("target endpoint not found")
		return r
	}

	r.BParser.ParseRequestBody(&r.httpReq.Response.Body)

	r.response = &Response{}
	r.BParser.Decode(&r.response)

	return r
}

func (r *Requester) Bind(responseValue interface{}) *Requester {
	err := helper.PairValues(r.response.Values, responseValue)
	if err != nil {
		r.BParser.Err = err
		return r
	}

	return r
}

func (r *Requester) BindArray(responseValue interface{}) *Requester {
	var arrayResponse globalModel.TArrayResponse

	err := helper.PairValues(r.response.Values, &arrayResponse)
	if err != nil {
		r.BParser.Err = err
		return r
	}

	err = helper.PairValues(arrayResponse.Data, responseValue)
	if err != nil {
		r.BParser.Err = err
		return r
	}

	return r
}

func (r *Requester) HasError() bool {
	if r.BParser.Err != nil || r.response.Error != nil {
		return true
	}
	return false
}

func (r *Requester) HandleError() {
	if r.BParser.Err != nil {
		BadRequest(r.c, r.BParser.Err)
		return

	} else if r.response.Error != nil {
		r.c.JSON(http.StatusOK, Response{
			Error: r.response.Error,
		})
		return
	}
}

func (r *Requester) Error() error {
	return r.BParser.Err
}

func (r *Requester) ErrorIs(targetError error) bool {
	var errorResponse Error
	err := helper.PairValues(r.response.Error, &errorResponse)
	if err != nil {
		r.BParser.Err = errors.New("error inside 'ErrorIs()': " + err.Error())
	}

	errorRefs, ok := errorResponse.Refs.(string)
	if ok && errorRefs == targetError.Error() {
		return true
	}

	return false
}

func (r *Requester) ErrorResponse() Error {
	if r.response.Error != nil {
		var errorResponse Error
		err := helper.PairValues(r.response.Error, &errorResponse)
		if err != nil {
			r.BParser.Err = err
			return Error{
				Refs: err,
			}
		}
		return errorResponse
	}
	return Error{}
}

func BadRequest(c *gin.Context, err error) {
	if err == nil {
		err = errors.New("")
	}

	c.JSON(http.StatusOK, Response{
		Error: Error{
			Code:    "400",
			Message: "Bad Request",
			Refs:    err.Error(),
		},
	})
}

func NotFound(c *gin.Context, err error) {
	if err == nil {
		err = errors.New("")
	}

	c.JSON(http.StatusOK, Response{
		Error: Error{
			Code:    "404",
			Message: "Not Found",
			Refs:    err.Error(),
		},
	})
}

func NotAuthorized(c *gin.Context, err error) {
	if err == nil {
		err = errors.New("")
	}

	c.JSON(http.StatusOK, Response{
		Error: Error{
			Code:    "401",
			Message: "Not Authorized",
			Refs:    err.Error(),
		},
	})
}

func JSONOk(c *gin.Context, value interface{}) {
	c.JSON(http.StatusOK, Response{
		ReturnVal: true,
		Values:    value,
	})
}

func JSONUpdate(c *gin.Context, isOK bool, value interface{}) {
	c.JSON(http.StatusOK, Response{
		ReturnVal: true,
		Values: bson.M{
			"updated": isOK,
			"docs":    value,
		},
	})
}

func JSONSuccess(c *gin.Context, isOK bool, value interface{}) {
	c.JSON(http.StatusOK, Response{
		ReturnVal: true,
		Values: bson.M{
			"success": isOK,
			"docs":    value,
		},
	})
}

func JSONCreate(c *gin.Context, isCreated, value interface{}, err error) {
	var strError interface{}

	if err == nil {
		strError = nil
	} else {
		strError = err.Error()
	}

	c.JSON(http.StatusOK, Response{
		ReturnVal: true,
		Values: bson.M{
			"created": isCreated,
			"docs":    value,
			"error":   strError,
		},
	})
}
