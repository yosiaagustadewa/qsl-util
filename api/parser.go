package api

import (
	"encoding/json"
	"errors"
	"gitlab.com/yosiaagustadewa/qsl-util/helper"
	"log"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

// Error struct
type Error struct {
	Level          string      `json:"level,omitempty" bson:"level,omitempty"`
	Code           string      `json:"code" bson:"code" binding:"required"`
	Type           string      `json:"type,omitempty" bson:"type,omitempty"`
	Status         string      `json:"status,omitempty" bson:"status,omitempty"`
	Message        string      `json:"message,omitempty" bson:"message,omitempty"`
	MessageDetails string      `json:"message_details,omitempty" bson:"message_details,omitempty"`
	Refs           interface{} `json:"refs,omitempty" bson:"refs,omitempty"`
}

// Request struct
type Request struct {
	Kind   string      `json:"kind" bson:"kind" binding:"required"`
	Values interface{} `json:"values" bson:"values" binding:"required"`
}

// Response struct
type Response struct {
	ReturnVal bool        `json:"returnval" bson:"returnval" binding:"required"`
	RespVal   string      `json:"respval,omitempty" bson:"respval,omitempty"`
	Kind      string      `json:"kind,omitempty" bson:"kind,omitempty"`
	Values    interface{} `json:"values,omitempty" bson:"values,omitempty"`
	Error     interface{} `json:"error,omitempty" bson:"error,omitempty"`
}

func (resp *Response) BindTo(v interface{}) error {
	if err := helper.PairValues(resp.Values, v); err != nil {
		return err
	}

	return nil
}

func (resp *Response) ParseError() Error {
	tempErr := Error{}
	err := helper.PairValues(resp.Error, &tempErr)
	if err != nil {
		return Error{
			Code:   "400",
			Status: "Bad Request",
			Refs:   err.Error(),
		}
	}
	return tempErr
}

// ParseRequest function
func parseRequest(c *gin.Context) (*Request, error) {
	var request *Request
	err := c.ShouldBindJSON(&request)
	if err != nil {
		return nil, err
	}

	return request, nil
}

func parseKind(c *gin.Context, kind string) (*Request, error) {
	req, err := parseRequest(c)
	if err != nil || strings.TrimSpace(req.Kind) != kind {
		return nil, errors.New("bad request")
	}
	return req, err
}

func ParseKindAndBody(c *gin.Context, kind string, result interface{}) error {
	req, err := parseKind(c, kind)
	if err != nil {
		return err
	}

	err = helper.PairValues(req.Values, result)
	if err != nil {
		return err
	}

	return nil
}

func ParseKindAndBodyArray(c *gin.Context, kind string, result *[]interface{}) error {
	req, err := parseKind(c, kind)
	if err != nil {
		return err
	}

	err = helper.PairValuesArray(req.Values, result)
	if err != nil {
		return err
	}

	return nil
}

// +++++++++++++++++++++++++++++++++++
// ATTRIBUTES
// +++++++++++++++++++++++++++++++++++

// Attributes type
type Attributes map[string]interface{}

// func (attr *Attributes) In(data interface{}, keyTarget string) {
//
// 	flattenedData, err := util.Flatten(data)
// }

// Match struct
type Match struct {
	Key   string      `json:"key" bson:"key" binding:"required"`
	Value interface{} `json:"value" bson:"value"`
}

// Like struct
type Like struct {
	Key   string `json:"key" bson:"key" binding:"required"`
	Value string `json:"value" bson:"value"`
}

// ElemMatch struct
type ElemMatch struct {
	Key   string   `json:"key" bson:"key" binding:"required"`
	Value []*Match `json:"value" bson:"value"`
}

// RangeValue struct
type RangeValue struct {
	Gt  interface{} `json:"gt" bson:"gt"`
	Lt  interface{} `json:"lt" bson:"lt"`
	Gte interface{} `json:"gte" bson:"gte"`
	Lte interface{} `json:"lte" bson:"lte"`
}

// Range struct
type Range struct {
	Key   string      `json:"key" bson:"key" binding:"required"`
	Value *RangeValue `json:"value" bson:"value"`
}

// In struct
type In struct {
	Key   string        `json:"key" bson:"key" binding:"required"`
	Value []interface{} `json:"value" bson:"value"`
}

// NotIn struct
type NotIn struct {
	Key   string        `json:"key" bson:"key" binding:"required"`
	Value []interface{} `json:"value" bson:"value"`
}

// All struct
type All struct {
	Key   string        `json:"key" bson:"key" binding:"required"`
	Value []interface{} `json:"value" bson:"value"`
}

// Filter struct
type Filter struct {
	Range []Range `json:"range" bson:"range"`
	In    []In    `json:"in" bson:"in"`
	NotIn []NotIn `json:"nin" bson:"nin"`
	All   []All   `json:"all" bson:"all"`
}

// Sort struct
type Sort struct {
	Key   string `json:"key" bson:"key" binding:"required"`
	Value string `json:"value" bson:"value"`
}

// Limit struct
type Limit struct {
	Rows   int `json:"rows" bson:"rows"`
	Offset int `json:"offset" bson:"offset"`
}

// Query struct
type Query struct {
	Match     []*Match     `json:"match" bson:"match"`
	Like      []*Like      `json:"like" bson:"like"`
	ElemMatch []*ElemMatch `json:"elem_match" bson:"elem_match"`
}

// Params struct
type Params struct {
	Query  Query       `json:"query" bson:"query"`
	Filter Filter      `json:"filter" bson:"filter"`
	Sort   []Sort      `json:"sort" bson:"sort"`
	Limit  Limit       `json:"limit" bson:"limit"`
	Data   interface{} `json:"data,omitempty" bson:"data,omitempty"`
}

// Get method
func (a *Attributes) Get(key string) interface{} {
	b, err := json.Marshal(a)
	if err != nil {
		return nil
	}
	var o map[string]interface{}
	err = json.Unmarshal(b, &o)
	if err != nil {
		return nil
	}
	return o[key]
}

// GetArrayInterface method
func (a *Attributes) GetArrayInterface(key string) []interface{} {
	if arr := a.Get(key); arr != nil {
		if values, ok := arr.([]interface{}); ok {
			return values
		}
	}
	return nil
}

// GetArrayString method
func (a *Attributes) GetArrayString(key string) (values []string) {
	if arr := a.GetArrayInterface(key); arr != nil {
		for _, v := range arr {
			if s, ok := v.(string); ok {
				values = append(values, s)
			}
		}
	}
	return values
}

// GetArrayInt method
func (a *Attributes) GetArrayInt(key string) (values []int) {
	if arr := a.GetArrayInterface(key); arr != nil {
		for _, v := range arr {
			if s, ok := v.(int); ok {
				values = append(values, s)
			}
		}
	}
	return values
}

// GetArrayFloat32 method
func (a *Attributes) GetArrayFloat32(key string) (values []float32) {
	if arr := a.GetArrayInterface(key); arr != nil {
		for _, v := range arr {
			if s, ok := v.(float32); ok {
				values = append(values, s)
			}
		}
	}
	return values
}

// GetArrayFloat64 method
func (a *Attributes) GetArrayFloat64(key string) (values []float64) {
	if arr := a.GetArrayInterface(key); arr != nil {
		for _, v := range arr {
			if s, ok := v.(float64); ok {
				values = append(values, s)
			}
		}
	}
	return values
}

// GetString method
func (a *Attributes) GetString(key string) string {
	b, err := json.Marshal(a)
	if err != nil {
		return ""
	}
	var o map[string]interface{}
	err = json.Unmarshal(b, &o)
	if err != nil {
		return ""
	}
	switch v := o[key].(type) {
	case string:
		return v
	case int:
		return strconv.Itoa(v)
	case float64:
		return strconv.FormatFloat(v, 'f', -1, 64)
	case float32:
		return strconv.FormatFloat(float64(v), 'f', -1, 64)
	default:
		return ""
	}
}

// GetInt method
func (a *Attributes) GetInt(key string) int {
	b, err := json.Marshal(a)
	if err != nil {
		return 0
	}
	var o map[string]interface{}
	err = json.Unmarshal(b, &o)
	if err != nil {
		return 0
	}
	switch v := o[key].(type) {
	case string:
		i, _ := strconv.Atoi(v)
		return i
	case int:
		return v
	case float64:
		return int(v)
	case float32:
		return int(v)
	default:
		return 0
	}
}

// GetFloat64 method
func (a *Attributes) GetFloat64(key string) float64 {
	b, err := json.Marshal(a)
	if err != nil {
		return 0
	}
	var o map[string]interface{}
	err = json.Unmarshal(b, &o)
	if err != nil {
		return 0
	}
	switch v := o[key].(type) {
	case string:
		i, _ := strconv.ParseFloat(v, 64)
		return i
	case int:
		return float64(v)
	case float64:
		return v
	case float32:
		return float64(v)
	default:
		return 0
	}
}

// Set method
func (a *Attributes) Set(key string, val interface{}) error {
	ba, err := json.Marshal(a)
	if err != nil {
		return err
	}
	var o map[string]interface{}
	err = json.Unmarshal(ba, &o)
	if err != nil {
		return err
	}
	if o == nil {
		o = make(map[string]interface{})
	}
	o[key] = val
	bo, err := json.Marshal(o)
	if err != nil {
		return err
	}
	err = json.Unmarshal(bo, &a)
	if err != nil {
		return err
	}
	return nil
}

// Delete method
func (a *Attributes) Delete(key string) error {
	ba, err := json.Marshal(a)
	if err != nil {
		return err
	}
	var o map[string]interface{}
	err = json.Unmarshal(ba, &o)
	if err != nil {
		return err
	}
	if o[key] != nil {
		delete(o, key)
	}
	bo, err := json.Marshal(o)
	if err != nil {
		return err
	}
	*a = nil
	err = json.Unmarshal(bo, &a)
	if err != nil {
		return err
	}
	return nil
}

// NewAttributes function
func NewAttributes(i interface{}) (*Attributes, error) {
	var a *Attributes
	ba, err := json.Marshal(i)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(ba, &a)
	if err != nil {
		return nil, err
	}

	return a, nil
}

// Error method
func (e Error) Error() error {
	b, err := json.Marshal(e)
	if err != nil {
		return errors.New("unknown error")
	}
	return errors.New(string(b))
}

// Err method
func (e Error) Err() string {
	b, err := json.Marshal(e)
	if err != nil {
		return "unknown error"
	}
	return string(b)
}

// Log method
func (e Error) Log() {
	b, err := json.Marshal(e)
	if err != nil {
		b = []byte("unknown error")
	}
	log.Println(string(b))
}
