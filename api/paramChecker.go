package api

import (
	"errors"
	"strings"

	"github.com/gin-gonic/gin"
)

func HandleParam(c *gin.Context, targetParams ...string) (*Param, error) {
	var param Param
	err := param.handle(c, targetParams...)
	return &param, err

}

type Param struct {
	List map[string]string
}

func (P *Param) handle(c *gin.Context, targetParams ...string) error {
	if targetParams == nil {
		return errors.New("nil param")
	}

	P.List = make(map[string]string, len(targetParams))
	var blackList []string
	for _, tParam := range targetParams {
		val := strings.TrimSpace(c.Param(tParam))
		if val == "" {
			blackList = append(blackList, val)
			continue
		}
		P.List[tParam] = val
	}

	if len(blackList) != 0 {
		return errors.New("param " + strings.Join(targetParams, ",") + " required")
	}

	return nil
}

func (P *Param) Get(param string) string {
	return P.List[param]
}
