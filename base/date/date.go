package date

import "gitlab.com/yosiaagustadewa/qsl-util/helper"

type TDate struct {
	TCreatedDate
	TModifiedDate
}

type TWrappedDate struct {
	DateAttribute TDate `bson:"date_attribute" json:"date_attribute"`
}

type TCreatedDate struct {
	CreatedAt string `bson:"created_at" json:"created_at"`
	CreatedBy string `bson:"created_by" json:"created_by"`
}

type TModifiedDate struct {
	ModifiedAt string `bson:"modified_at" json:"modified_at"`
	ModifiedBy string `bson:"modified_by" json:"modified_by"`
}

type TWrappedModifiedDate struct {
	DateAttribute TModifiedDate `bson:"date_attribute" json:"date_attribute"`
}

func CreateDate(creator string) TWrappedDate {
	return TWrappedDate{
		DateAttribute: TDate{
			TCreatedDate: TCreatedDate{
				CreatedAt: helper.GetJSUnixTime(),
				CreatedBy: creator,
			},
			TModifiedDate: TModifiedDate{},
		},
	}
}

func UpdateDate(modifier string) TWrappedModifiedDate {
	return TWrappedModifiedDate{
		DateAttribute: TModifiedDate{
			ModifiedAt: helper.GetJSUnixTime(),
			ModifiedBy: modifier,
		},
	}
}
