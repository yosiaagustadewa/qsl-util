package db

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

func NewMongoConnection(ctx context.Context, uri string) *mongo.Client {
	Client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatal(err)
	}

	err = Client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	return Client
}

//func NewMongoClient() *mongo.Client {
//	return db.NewMongoConnection(context.Background(), config.MongoAtlasURI)
//}

//func DBConnect(ctx context.Context, client *mongo.Client, DBNAME string) *mongo.Database {
//	if err := client.Connect(ctx); err != nil {
//		log.Fatal(err)
//	}
//
//	log.Println("DB", DBNAME, "Connected")
//
//
//	return client.Database(DBNAME)
//}
