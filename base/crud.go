package base

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var errNotUpdated = errors.New("not updated")
var errNotDeleted = errors.New("not deleted")

type basicCRUD struct {
	ctx  context.Context
	Coll *mongo.Collection
}

func UseBasicCRUD(ctx context.Context, collection *mongo.Collection) BasicCRUDer {
	// return &basicCRUD{collection}
	return BasicCRUDer(&basicCRUD{
		ctx,
		collection,
	})
}

func UseTransactionBasicCRUD(ctx context.Context, Client *mongo.Client, DBName, COLLName string) BasicCRUDer {
	collection := Client.Database(DBName).Collection(COLLName)
	return BasicCRUDer(&basicCRUD{
		ctx,
		collection,
	})
}

type BasicCRUDer interface {
	// GET
	GetByID(ID string, result interface{}) error
	GetByIDs(IDs []string, result interface{}) error
	GetByIDsToMaps(IDs []string, result *[]map[string]interface{}) error

	// CREATE
	Create(Payload interface{}) error

	// UPDATE
	UpdateByID(ID string, query interface{}) error
	UpdateAndGetByID(ID string, query interface{}, result interface{}) error

	// DELETE
	DeleteByID(ID string) error
}

func (b *basicCRUD) GetByID(ID string, result interface{}) error {
	res := b.Coll.FindOne(b.ctx, bson.M{"id": ID})

	err := res.Decode(result)
	if err != nil {
		return err
	}
	return nil
}

func (b *basicCRUD) GetByIDs(IDs []string, result interface{}) error {
	cursor, err := b.Coll.Find(b.ctx, bson.M{"id": bson.M{"$in": IDs}})
	if err != nil {
		return err
	}

	if cursor.RemainingBatchLength() == 0 {
		return err
	}

	if cursor.All(b.ctx, result) != nil {
		return err
	}

	return nil
}

func (b *basicCRUD) GetByIDsToMaps(IDs []string, result *[]map[string]interface{}) error {
	cursor, err := b.Coll.Find(b.ctx, bson.M{"id": bson.M{"$in": IDs}})
	if err != nil {
		return err
	}

	if cursor.RemainingBatchLength() == 0 {
		return err
	}

	for cursor.Next(b.ctx) {
		r := make(map[string]interface{})
		if err = cursor.Decode(&r); err != nil {
			return err
		}

		*result = append(*result, r)
	}

	return nil
}

func (b *basicCRUD) Create(Payload interface{}) error {

	_, err := b.Coll.InsertOne(b.ctx, Payload)
	if err != nil {
		return err
	}

	return nil
}

func (b *basicCRUD) UpdateByID(ID string, query interface{}) error {

	updateResult, err := b.Coll.UpdateOne(b.ctx, bson.D{{Key: "id", Value: ID}}, query)
	if err != nil {
		return err
	}

	if updateResult.MatchedCount == 0 {
		return errNotUpdated
	}

	return nil
}

func (b *basicCRUD) UpdateAndGetByID(ID string, query interface{}, result interface{}) error {

	var opt options.FindOneAndUpdateOptions
	opt.SetReturnDocument(1)

	err := b.Coll.FindOneAndUpdate(b.ctx, bson.D{{"id", ID}}, query, &opt).Decode(result)
	if err != nil {
		return err
	}

	return nil
}

func (b *basicCRUD) DeleteByID(ID string) error {
	deleteResult, err := b.Coll.DeleteOne(b.ctx, bson.D{{"id", ID}})
	if err != nil {
		return err
	}

	if deleteResult.DeletedCount == 0 {
		return errNotDeleted
	}

	return nil
}
