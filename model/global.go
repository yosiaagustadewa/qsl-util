package model

import (
	"errors"
	"strconv"
	"strings"
	"time"
)

var ErrBadRequest = errors.New("Bad request")
var ErrNotModified = errors.New("Not Modified")
var ErrNotFound = errors.New("Not found")
var ErrNotAuthorized = errors.New("Not authorized")

var ErrRoleNotFound = errors.New("Member role not found")

type TUpdateDateQuery struct {
	DateAttributes TUpdateDateAttributes `bson:"date_attributes" json:"date_attributes" binding:"required"`
}

type TUpdateDateAttributes struct {
	DateModified string `bson:"date_modified,omitempty" json:"date_modified,omitempty"`
	ModifiedBy   string `bson:"modified_by,omitempty" json:"modified_by,omitempty"`
}

type TDateAttributes struct {
	DateCreated  string `bson:"date_created" json:"date_created,omitempty"`
	DateModified string `bson:"date_modified" json:"date_modified,omitempty"`
	CreatedBy    string `bson:"created_by" json:"created_by,omitempty"`
	ModifiedBy   string `bson:"modified_by" json:"modified_by,omitempty"`
}

type TArrayResponse struct {
	Data interface{} `bson:"data" json:"data"`
}

func (T *TDateAttributes) IsCreatedBy(setterCID string) {
	var date time.Time
	T.DateCreated = strconv.FormatInt(date.UTC().UnixNano(), 10)
	T.CreatedBy = setterCID
}

func (T *TUpdateDateAttributes) IsModifiedBy(setterCID string) {
	var date time.Time
	T.DateModified = strconv.FormatInt(date.UTC().UnixNano(), 10)
	T.ModifiedBy = setterCID

	return
}

// default "02 Jan 2006 15:04:00"
func (T *TDateAttributes) GetDateCreated(formatLayout ...string) string {
	numericFullDate, _ := strconv.ParseInt(T.DateCreated, 10, 64)
	t := time.Unix(numericFullDate/1000, 0)

	if len(formatLayout) != 0 && strings.TrimSpace(formatLayout[0]) != "" {
		return t.Format(formatLayout[0])
	}
	return t.Format("02 Jan 2006 15:04:00")
}

func (T *TDateAttributes) GetDateModified(formatLayout ...string) string {
	numericFullDate, _ := strconv.ParseInt(T.DateCreated, 10, 64)
	t := time.Unix(numericFullDate/1000, 0)

	if len(formatLayout) != 0 && strings.TrimSpace(formatLayout[0]) != "" {
		return t.Format(formatLayout[0])
	}
	return t.Format("02 Jan 2006 15:04:00")
}

// ========================================== SORT =======================================================

type TSortList struct {
	Sort map[string]int `json:"sort" bson:"sort" binding:"required"`
}

// ======================================== PAGINATION ===================================================

type TPagination struct {
	Page  int64 `json:"page" bson:"page" binding:"required"`
	Limit int64 `json:"limit" bson:"limit" binding:"required"`
}
