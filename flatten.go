package util

import (
	"encoding/json"
	"errors"
	"regexp"
)

var NotValidInputError = errors.New("Not a valid input: map or slice")

func Flatten(nested map[string]interface{}) (map[string]interface{}, error) {
	flatmap := make(map[string]interface{})

	err := flatten(true, flatmap, nested, "", ".")
	if err != nil {
		return nil, err
	}

	return flatmap, nil
}

var NotValidJsonInputError = errors.New("Not a valid input, must be a map")

var isJsonMap = regexp.MustCompile(`^\s*\{`)

func FlattenString(nestedstr string) (string, error) {
	if !isJsonMap.MatchString(nestedstr) {
		return "", NotValidJsonInputError
	}

	var nested map[string]interface{}
	err := json.Unmarshal([]byte(nestedstr), &nested)
	if err != nil {
		return "", err
	}

	flatmap, err := Flatten(nested)
	if err != nil {
		return "", err
	}

	flatb, err := json.Marshal(&flatmap)
	if err != nil {
		return "", err
	}

	return string(flatb), nil
}

func flatten(top bool, flatMap map[string]interface{}, nested interface{}, prefix string, style string) error {
	assign := func(newKey string, v interface{}) error {
		switch v.(type) {
		case map[string]interface{}:
			if err := flatten(false, flatMap, v, newKey, style); err != nil {
				return err
			}
		default:
			flatMap[newKey] = v
		}

		return nil
	}

	switch nested.(type) {
	case map[string]interface{}:
		for k, v := range nested.(map[string]interface{}) {
			newKey := enkey(top, prefix, k, style)
			assign(newKey, v)
		}
	default:
		return NotValidInputError
	}

	return nil
}

func enkey(top bool, prefix, subkey string, style string) string {
	key := prefix

	if top {
		key += subkey
	} else {
		key += style + subkey
	}

	return key
}
