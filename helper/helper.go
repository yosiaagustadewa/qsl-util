package helper

import (
	"encoding/json"
	"errors"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator"
	validator2 "gopkg.in/go-playground/validator.v9"

	globalModel "gitlab.com/yosiaagustadewa/qsl-util/model"
	"go.mongodb.org/mongo-driver/bson"
)

func ValidateRole(userRole string, grantedRoles ...string) error {
	permitted := false
	for _, role := range grantedRoles {
		if userRole == role {
			permitted = true
			break
		}
	}
	if !permitted {
		return errors.New("user is not allowed to do this action")
	}
	return nil
}

func WrapRequest(kind string, values interface{}) interface{} {
	return bson.M{
		"kind":   kind,
		"values": values,
	}
}

func WrapToArr(results interface{}) interface{} {
	return bson.M{
		"data": results,
	}
}

func WErr(errPayload string) error {
	return errors.New(errPayload)
}

func GetJSUnixTime() string {
	return dateToJSUnix(time.Now().UTC())
}

func dateToJSUnix(date time.Time) string {
	dateString := strconv.FormatInt(date.UnixNano(), 10)
	dateRune := []rune(dateString)[0:13]

	return string(dateRune)
}

func jsUnixToDate(jsString string) (time.Time, error) {

	dateRawRune := []rune(jsString)

	// splitting date
	dateInt, err := strconv.ParseInt(string(dateRawRune[0:10]), 10, 64)
	if err != nil {
		return time.Now().UTC(), err
	}

	// splitting nSec
	nSecInt, err := strconv.ParseInt(string(dateRawRune[10:13])+"000000", 10, 64)
	if err != nil {
		return time.Now().UTC(), err
	}

	// convert to date object
	return time.Unix(dateInt, nSecInt).UTC(), nil
}

func JSUnixToDateString(jsUnix string, result *string) error {
	t, err := jsUnixToDate(jsUnix)
	if err != nil {
		return errors.New("invalid date")
	}
	*result = t.Format("02 Jan 2006 15:04:05")
	return err
}

func JSUnixToFullDateStringDefault(jsUnix, defaultVal string) string {
	t, err := jsUnixToDate(jsUnix)
	if err != nil {
		return defaultVal
	}
	return t.Format("02 Jan 2006 15:04:05")
}

func JSUnixToDateStringDefault(jsUnix, defaultVal string) string {
	t, err := jsUnixToDate(jsUnix)
	if err != nil {
		return defaultVal
	}
	return t.Format("02 Jan 2006")
}

func JSUnixToTimeStringDefault(jsUnix, defaultVal string) string {
	t, err := jsUnixToDate(jsUnix)
	if err != nil {
		return defaultVal
	}
	return t.Format("15:04:05")
}

func JSUnixToTimeFormatDefault(jsUnix, format, defaultVal string) string {
	t, err := jsUnixToDate(jsUnix)
	if err != nil {
		return defaultVal
	}
	return t.Format(format)
}

func NewOID() string {
	return strings.ToUpper(primitive.NewObjectID().Hex())
}

func GetCID(c *gin.Context) (string, error) {

	CID := c.GetHeader("X-Token-Credential")
	//if CID == "" {

	// ==================== DEBUG ONLY ==================== //
	if CID == "" {
		CID = "200812085131DE07D1GK55XT"
		// CID = "210111092615C476315LAAA3"
		return CID, nil
	}
	// ==================== DEBUG ONLY ==================== //

	//return "", errors.New("credential not found")
	//}

	return CID, nil
}

// func (a Attributes) ParseKeyValSetAttributes() Attributes {
//	if a != nil {
//		var attributes = make(Attributes)
//		var key string
//		ParseSubAttributes(a, attributes, &key)
//		return attributes
//	}
//
//	return nil
// }
//
// // ParseSubAttributes function
// func ParseSubAttributes(i, o Attributes, key *string) {
//	for i, v := range i {
//		var attributes = make(Attributes)
//		err := api.PairValues(v, &attributes)
//		if err == nil {
//			if *key != "" {
//				k := *key + "." + i
//				ParseSubAttributes(attributes, o, &k)
//			} else {
//				ParseSubAttributes(attributes, o, &i)
//			}
//		} else {
//			if *key != "" {
//				o[*key+"."+i] = v
//			} else {
//				o[i] = v
//			}
//		}
//	}
// }

var validate *validator.Validate

// BindValidate function
func BindValidate(o interface{}) error {
	ginValidate, ok := binding.Validator.Engine().(*validator2.Validate)
	if !ok {
		err := binding.Validator.ValidateStruct(o)
		if err != nil {
			return err
		}
		validate = validator.New()
		err = validate.Struct(o)

		return err
	}

	err := ginValidate.Struct(o)
	if err != nil {
		return err
	}

	validate = validator.New()
	err = validate.Struct(o)

	return err
}

// PairValues function
func PairValues(i, o interface{}) error {
	if i == nil {
		return errors.New("error while pair values, values is nil")
	}
	b, err := json.Marshal(i)
	if err != nil {
		return err
	}
	err = json.Unmarshal(b, &o)
	if err != nil {
		return err
	}

	// check type of o
	r := reflect.ValueOf(o)
	if r.Kind() == reflect.Ptr && !r.IsNil() {
		r = r.Elem()
	}
	if r.Kind() != reflect.Struct && r.Kind() != reflect.Interface {

		return nil
	}

	// validate struct :
	err = BindValidate(o)
	if err != nil {

		return err
	}

	return nil
}

// PairValues function
func PairValuesArray(in interface{}, out *[]interface{}) error {
	var arr globalModel.TArrayResponse
	err := PairValues(in, &arr)
	if err != nil {
		return err
	}

	err = PairValues(arr.Data, out)
	if err != nil {
		return err
	}

	return nil
}

/*
 * -1 = default
 * 1 ~ n = area
 */
func ParseCallSignArea(cs string) (string, string) {
	res := strings.Split(cs, "/")
	if len(res) > 1 {
		return res[0], res[1]
	}
	return res[0], "-1"
}

// =====================================================================================================================
// ================================================= CONCURRENT ========================================================
// =====================================================================================================================

func NewSpreader(nConcurrent int) *spreader {
	s := new(spreader)
	s.Init(nConcurrent)
	return s
}

type spreader struct {
	errChannel  chan error
	nConcurrent int
}

func (s *spreader) Init(nConcurrent int) {
	s.nConcurrent = nConcurrent
	s.errChannel = make(chan error, s.nConcurrent)
}

func (s *spreader) C() chan error {
	return s.errChannel
}

func (s *spreader) ListenError() error {
	var err error
	arrivedCount := 0
	for done := false; !done; {
		select {
		case e := <-s.errChannel:
			arrivedCount += 1

			if err == nil && e != nil {
				err = e
			}
			if arrivedCount == s.nConcurrent {
				close(s.errChannel)
				done = true
			}
		}
	}
	return err
}
