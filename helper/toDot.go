package helper

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"reflect"
)

// STRUCT TO DOT NOTATION
func DotStruct(removeZero bool, structObj interface{}) (bson.M, error) {
	var inBSON bson.M
	err := PairValues(structObj, &inBSON)
	if err != nil {
		return bson.M{}, err
	}

	finalMap := make(map[string]interface{})
	parseMap(removeZero, "", inBSON, &finalMap)

	return finalMap, nil
}

// BSON TO DOT NOTATION
func DotBSON(removeZero bool, aMap map[string]interface{}) bson.M {
	finalMap := make(map[string]interface{})
	parseMap(removeZero, "", aMap, &finalMap)
	return finalMap
}

func parseMap(removeZero bool, k string, aMap map[string]interface{}, finalMap *map[string]interface{}) {
	if len(aMap) == 0 {
		(*finalMap)[k] = nil
		return
	}

	for key, val := range aMap {
		if val != nil {
			switch concreteVal := val.(type) {
			case map[string]interface{}:
				parseMap(removeZero, getKey(k, key), val.(map[string]interface{}), finalMap)
			case []interface{}:
				(*finalMap)[getKey(k, key)] = val.([]interface{})
			default:
				concreteValType := reflect.TypeOf(concreteVal)
				if concreteValType.Kind() == reflect.Map {
					parseMap(removeZero, getKey(k, key), concreteVal.(primitive.M), finalMap)
				} else {
					if removeZero && reflect.ValueOf(concreteVal).IsZero() {
						continue
					}
					(*finalMap)[getKey(k, key)] = concreteVal
				}
			}
		} else {
			(*finalMap)[getKey(k, key)] = nil
		}
	}
}

func getKey(k string, key string) string {
	if k == "" {
		return key
	}
	return k + "." + key
}
